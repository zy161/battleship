package project1.battleship;

import java.util.ArrayList;
import java.util.HashMap;

public interface Board<T> {
    int getWidth();
    int getHeight();
    String tryAddShip(Ship<T> toAdd);
    T whatIsAtForSelf(Coordinate where);
    T whatIsAtForEnemy(Coordinate where);
    Ship<T> fireAt(Coordinate c);
    boolean checkLose();
    Ship<T> getShipAt(Coordinate c);
    String removeShip(Ship<T> s);
    HashMap<String, Integer> sonarScan(Coordinate c);
    ArrayList<Coordinate> getSonarArea(Coordinate c);
    boolean isCoordinateOnBoard(Coordinate c);
}
