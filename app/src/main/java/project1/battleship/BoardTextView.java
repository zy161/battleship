package project1.battleship;

import java.util.function.Function;

/**
 * This class handles textual display of
 * a Board (i.e., converting it to a string to show
 * to the user).
 * It supports two ways to display the Board:
 * one for the player's own board, and one for the
 * enemy's board.
 */
public class BoardTextView {
    /**
     * The Board to display
     */
    private final Board<Character> toDisplay;
    /**
     * Constructs a BoardView, given the board it will display.
     *
     * @param toDisplay is the Board to display
     * @throws IllegalArgumentException if the board is larger than 10x26.
     */
    public BoardTextView(Board<Character> toDisplay) {
        this.toDisplay = toDisplay;
        if (toDisplay.getWidth() > 10 || toDisplay.getHeight() > 26) {
            throw new IllegalArgumentException(
                    "Board must be no larger than 10x26, but is " + toDisplay.getWidth() + "x" + toDisplay.getHeight());
        }
    }
    /**
     * make the board's text view
     * @param getSquareFn to display different ships based on the type of the ship
     * @return the string of board's text view
     */
    protected String displayAnyBoard(Function<Coordinate, Character> getSquareFn) {
        String myBoard;
        String header = makeHeader();
        StringBuilder ans = new StringBuilder(); // README shows two spaces at
        String sep="|"; //start with nothing to separate, then switch to | to separate
        char row_index = 'A';
        for (int row = 0; row < toDisplay.getHeight(); row++) {
            ans.append(row_index);
            ans.append(" ");
            for(int column = 0; column < toDisplay.getWidth(); column++){
                if(getSquareFn.apply(new Coordinate(row,column)) != null){
                    ans.append(getSquareFn.apply(new Coordinate(row, column)));
                }
                else{
                    ans.append(" ");
                }
                if(column < toDisplay.getWidth() - 1){
                    ans.append(sep);
                }
            }
            ans.append(" ");
            ans.append(row_index);
            ans.append("\n");
            row_index++;
        }
        String body = ans.toString();
        myBoard = header + body + header;
        return myBoard;
    }
    /**
     * This makes the header line, e.g. 0|1|2|3|4\n
     *
     * @return the String that is the header line for the given board
     */
    String makeHeader() {
        StringBuilder ans = new StringBuilder("  "); // README shows two spaces at
        String sep=""; //start with nothing to separate, then switch to | to separate
        for (int i = 0; i < toDisplay.getWidth(); i++) {
            ans.append(sep);
            ans.append(i);
            sep = "|";
        }
        ans.append("\n");
        return ans.toString();
    }
    /**
     * display the player's board text view
     * @return the player's board's text view
     */
    public String displayMyOwnBoard() {
        return displayAnyBoard((c)->toDisplay.whatIsAtForSelf(c));
    }
    /**
     * display the enemy's board text view
     * @return the enemy's board's text view
     */
    public String displayEnemyBoard(){
        return displayAnyBoard((c)->toDisplay.whatIsAtForEnemy(c));
    }
    /**
     * display the player and enemy's board text views
     * @param enemyView enemy's board's text view
     * @param myHeader a string of the player's header
     * @param enemyHeader a string of the enemy's header
     * @return the players board's text views
     */
    public String displayMyBoardWithEnemyNextToIt(BoardTextView enemyView, String myHeader, String enemyHeader) {
        String [] myLines = this.displayMyOwnBoard().split("\n");
        String [] enemyLines = enemyView.displayEnemyBoard().split("\n");
        StringBuilder displayBoards = new StringBuilder();
        String whiteSpace = " ";
        int boardWidth = this.toDisplay.getWidth();
        int headerDistance = 2 * boardWidth + 22 - 5;
        int boardDistance = 2 * boardWidth + 19 - 5 - myHeader.length();
        //the first header starts at column number 5
        displayBoards.append("     ");
        displayBoards.append(myHeader);
        displayBoards.append(whiteSpace.repeat(headerDistance));
        displayBoards.append(enemyHeader);
        displayBoards.append('\n');
        //the column index rows need two extra spaces
        displayBoards.append(myLines[0]);
        displayBoards.append(whiteSpace.repeat(boardDistance + 2));
        displayBoards.append(enemyLines[0]);
        displayBoards.append('\n');
        for(int i = 1; i < myLines.length - 1; i++){
            displayBoards.append(myLines[i]);
            displayBoards.append(whiteSpace.repeat(boardDistance));
            displayBoards.append(enemyLines[i]);
            displayBoards.append('\n');
        }
        displayBoards.append(myLines[myLines.length - 1]);
        displayBoards.append(whiteSpace.repeat(boardDistance + 2));
        displayBoards.append(enemyLines[enemyLines.length - 1]);
        displayBoards.append('\n');
        return displayBoards.toString();
    }

}
