package project1.battleship;

import java.io.IOException;

public class Placement {
    private final Coordinate where;
    private final char orientation;
    public Placement(Coordinate c, char o){
        this.where = c;
        this.orientation = Character.toUpperCase(o);
    }
    public Coordinate getCoordinate(){
        return where;
    }
    public char getOrientation(){
        return orientation;
    }
    /**
     * constructor for placement
     * @param place the string that contains placement information
     */
    public Placement(String place) {
        String init = place.toUpperCase();
        if(init.length() != 3){
            throw new IllegalArgumentException("Invalid input, which should be three character in the string!");
        }
        Coordinate c = new Coordinate(place.substring(0,2));
        where = c;
        char dir = init.charAt(2);
        if(dir != 'H' && dir != 'V' && dir != 'U' && dir != 'R' && dir != 'D' && dir != 'L'){
            throw new IllegalArgumentException("Invalid input, which should contain the correct direction in the string!");
        }
        orientation = dir;
    }
    @Override
    public boolean equals(Object o) {
        if (o.getClass().equals(getClass())) { //Not 'o instanceof Placement' because if we later made a subclass of Placement
            Placement p = (Placement) o; //we might get them as equal incorrectly
            return where.equals(p.where) && orientation == p.orientation; //implicitly deploy Coordinate class's equals function
        }
        return false;
    }
    @Override
    public String toString() {
        return "("+where.toString()+", " + orientation+")";
    }
    @Override
    public int hashCode() {
        return toString().hashCode();
    }
}
