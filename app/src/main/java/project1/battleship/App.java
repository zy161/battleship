/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package project1.battleship;

import java.io.*;

public class App {
    private final TextPlayer player1;
    private final TextPlayer player2;
    public App(TextPlayer playerA, TextPlayer playerB){
        this.player1 = playerA;
        this.player2 = playerB;
    }
    /**
     * Do new Placements and add ships to the board and the toDisplay in view
     */
    public void doPlacementPhase() throws IOException{
        player1.doPlacementPhase();
        player2.doPlacementPhase();
    }
    /**
     * let player1 play a turn, then see if player 2 has lost. Then let player 2 play a turn and see if player 1 has lost.
     * It should repeat this until one player has lost, then report the outcome.
     * @throws IOException
     */

    public void doAttackingPhase() throws IOException{
        while(true){
            player1.playOneTurn(player2.theBoard, player2.name);
            if(player2.theBoard.checkLose()){
                System.out.println("Great, Player " + player1.name + " you win!");
                break;
            }
            player2.playOneTurn(player1.theBoard, player1.name);
            if(player1.theBoard.checkLose()){
                System.out.println("Great, Player " + player2.name + " you win!");
                break;
            }
        }
    }
    /**
     * Create a 10x20 board, and call doOnePlacement to place a ship.
     * InputStreamReader(System.in) is the input, System.out is the output
     * for the output.
     * @param inputReader read from the system.in to get the input
     * @throws IOException when encountering some reading errors
     */
    private static String checkMode(BufferedReader inputReader) throws IOException{
        while(true){
            try{
                String mode = inputReader.readLine().toUpperCase();
                if(mode.equals("A") || mode.equals("B") || mode.equals("C") || mode.equals("D")){
                    return mode;
                }
                else{
                    System.out.println("This is not a valid mode. Please select the mode provided!");
                }
            }
            catch(IllegalArgumentException e){
                System.out.println(e.getMessage());
            }
        }
    }
    /**
     * Create a 10x20 board, and call doOnePlacement to place a ship.
     * InputStreamReader(System.in) is the input, System.out is the output
     * for the output.
     * @param args nothing
     * @throws IOException when encountering some reading and printing errors
     */
    public static void main(String[] args) throws IOException{
        //make main create a 10x20 board
        Board<Character> b1 = new BattleShipBoard<>(10, 20, 'X');
        Board<Character> b2 = new BattleShipBoard<>(10, 20, 'X');
        //create two players, and they need to share a BufferedReader, and System.out
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        V1ShipFactory factory1 = new V1ShipFactory();
        V2ShipFactory factory2 = new V2ShipFactory();
        //Read a prompt and let the player choose which mode to play
        System.out.println("Which mode do you want to play?");
        System.out.print("Press A for human vs human\n" +
                         "Press B for human vs computer\n" +
                         "Press C for computer vs human\n" +
                         "Press D for computer vs computer\n");
        TextPlayer p1 = null;
        TextPlayer p2 = null;
        String choice = checkMode(input);
        //create players based on the selection
        if(choice.equals("A")){
            p1 = new TextPlayer("A", b1, input, System.out, factory1, factory2);
            p2 = new TextPlayer("B", b2, input, System.out, factory1, factory2);
        }
        else if(choice.equals("B")){
            p1 = new TextPlayer("A", b1, input, System.out, factory1, factory2);
            p2 = new ComputerPlayer("Computer B", b2, input, System.out, factory1, factory2);
        }
        else if(choice.equals("C")){
            p1 = new ComputerPlayer("Computer A", b1, input, System.out, factory1, factory2);
            p2 = new TextPlayer("B", b2, input, System.out, factory1, factory2);
        }
        else{
            p1 = new ComputerPlayer("Computer A", b1, input, System.out, factory1, factory2);
            p2 = new ComputerPlayer("Computer B", b2, input, System.out, factory1, factory2);
        }
        //create an App with players
        App myapp = new App(p1, p2);
        //call doOnePlacement on that app
        myapp.doPlacementPhase();
        myapp.doAttackingPhase();
    }

}
