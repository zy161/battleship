package project1.battleship;

import java.util.HashMap;
import java.util.HashSet;
import java.lang.*;

public class RectangleShip<T> extends BasicShip<T> {
    final String name;
    public String getName(){
        return name;
    }
    public RectangleShip(String name, Coordinate upperLeft, int width, int height, ShipDisplayInfo<T> myDisplayInfo, ShipDisplayInfo<T> enemyDisplayInfo) {
        super(makeCoords(upperLeft, width, height), myDisplayInfo, enemyDisplayInfo, makeIndexes(upperLeft, width, height));
        this.name = name;
    }
    public RectangleShip(String name, Coordinate upperLeft, int width, int height, T data, T onHit) {
        this(name, upperLeft, width, height, new SimpleShipDisplayInfo<T>(data, onHit), new SimpleShipDisplayInfo<T>(null, data));
    }
    //for convenience
    public RectangleShip(Coordinate upperLeft, T data, T onHit) {
        this("testship", upperLeft, 1, 1, data, onHit);
    }
    /**
     * make a coordinates hash set based on the given elements
     * @param upperLeft the ship's upperleft coordinate
     * @param width the ship's width
     * @param height the ship's height
     * @return the hash set of coordinates that the ship occupies
     */
    static HashSet<Coordinate> makeCoords(Coordinate upperLeft, int width, int height){
        HashSet<Coordinate> myCoords = new HashSet<>();
        myCoords.add(upperLeft);
        for(int r = 0; r < height; ++r){
            for(int c = 0; c < width; ++c){
                Coordinate c1 = new Coordinate(upperLeft.getRow() + r, upperLeft.getColumn() + c);
                myCoords.add(c1);
            }
        }
        return myCoords;
    }

    /**
     * make every coordinate in the ship has its corresponding index
     * @param upperLeft the ship's upperleft coordinate
     * @param width the ship's width
     * @param height the ship's height
     * @return the hash map of coordinates that the ship occupies and the corresponding index
     */
    static HashMap<Coordinate, Integer> makeIndexes(Coordinate upperLeft, int width, int height){
        HashMap<Coordinate, Integer> myIndex = new HashMap<>();
        int index = 0;
        //for submarine, it's '1 2' or '1\n 2'
            for(int i = 0; i < height; i++){
                for(int j = 0; j < width; j++){
                    Coordinate c = new Coordinate(upperLeft.getRow() + i, upperLeft.getColumn() + j);
                    myIndex.put(c, index);
                    index++;
                }
            }
        return myIndex;
    }
}
