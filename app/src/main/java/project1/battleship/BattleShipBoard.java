package project1.battleship;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class BattleShipBoard<T> implements Board<T>{
    private final int width;
    private final int height;
    private final PlacementRuleChecker<T> placementChecker;
    private final ArrayList<Ship<T>> myShips;
    HashSet<Coordinate> enemyMisses;
    final T missInfo;
    public int getWidth(){
        return width;
    }
    public int getHeight(){
        return height;
    }
    /**
     * Constructs a BattleShipBoard with the specified width
     * and height
     * @param w is the width of the newly constructed board.
     * @param h is the height of the newly constructed board.
     * @throws IllegalArgumentException if the width or height are less than or equal to zero.
     */
    public BattleShipBoard(int w, int h, PlacementRuleChecker<T> p, T miss){
        if (w <= 0) {
            throw new IllegalArgumentException("BattleShipBoard's width must be positive but is " + w);
        }
        if (h <= 0) {
            throw new IllegalArgumentException("BattleShipBoard's height must be positive but is " + h);
        }
        this.width = w;
        this.height = h;
        this.placementChecker = p;
        this.myShips = new ArrayList<>();
        this.enemyMisses = new HashSet<>();
        this.missInfo = miss;
    }
    //for convenience
    public BattleShipBoard(int w, int h, T miss) {
        this(w, h, new InBoundsRuleChecker<T>(new NoCollisionRuleChecker<>(null)), miss);
    }
    /**
     * try to add a ship to the board and check the feasibility
     * @param toAdd the ship that wants to be added on the board
     * @return the error message if the adding fails, nothing otherwise
     */
    public String tryAddShip(Ship<T> toAdd){
        String placementProblem = placementChecker.checkPlacement(toAdd, this);
        if(placementProblem == null){
            myShips.add(toAdd);
            return null;
        }
        else{
            throw new IllegalArgumentException(placementProblem);
        }
    }

    //for my own view display, data if not hit, onHit if hit
    @Override
    public T whatIsAtForSelf(Coordinate where) {
        return whatIsAt(where, true);
    }
    //for the enemy view, nothing if not hit, data if hit
    @Override
    public T whatIsAtForEnemy(Coordinate where) {
        return whatIsAt(where, false);
    }
    //takes a Coordinate, and sees which (if any) Ship occupies that coordinate.
    protected T whatIsAt(Coordinate where, boolean isSelf){
        if(enemyMisses.contains(where) && !isSelf){
            return missInfo;
        }
        for (Ship<T> s: myShips) {
            if (s.occupiesCoordinates(where)){
                return s.getDisplayInfoAt(where, isSelf);
            }
        }
        return null;
    }
    /**
     * fire at the given coordinate and record a hit at the ship if it's on the given coordinate
     * @param c the given coordinate, check if it's occupied by a ship first
     * @return the ship that's been hit
     */
    public Ship<T> fireAt(Coordinate c){
        //search for any ship that occupies coordinate c
        for (Ship<T> s: myShips) {
            if (s.occupiesCoordinates(c)) {
                //If one is found, that Ship is "hit" by the attack and should record it
                s.recordHitAt(c);

                //return the hit ship
                return s;
            }
        }
        //If no ship is at this coordinate, we should record the miss in the enemyMisses HashSet
        enemyMisses.add(c);
        return null;
    }
    /**
     * check sunk status of all the ships on the board
     * @return boolean if the board's player loses or not
     */
    public boolean checkLose(){
        for(Ship<T> s : myShips){
            if(!s.isSunk()){
                return false;
            }
        }
        return true;
    }
    /**
     * get a ship that occupies the given coordinate
     * @param c the given coordinate, check if it's occupied by a ship
     * @return ship that occupies the given coordinate, null if there's nothing
     */
    public Ship<T> getShipAt(Coordinate c){
        for (Ship<T> s: myShips){
            if (s.occupiesCoordinates(c)){
                return s;
            }
        }
        return null;
    }
    /**
     * remove the given ship from the board
     * @param s the ship that's going to be removed, check if it's on the board first
     * @return nothing if successfully removed, error message if the ship given is invalid
     */
    public String removeShip(Ship<T> s){
        if(myShips.contains(s)){
            myShips.remove(s);
            return null;
        }
        else{
            return ("Invalid removal, we do not have this ship!");
        }
    }
    //scan area:                          *
    //                                   ***
    //                                  *****
    //                                 ***C***
    //                                  *****
    //                                   ***
    //                                    *
    /**
     * calculate the scan area based on the given coordinate
     * @param c the given coordinate, get the scan area around it
     * @return the array of coordinates of the sonar scan area
     */
    public ArrayList<Coordinate> getSonarArea(Coordinate c){
        ArrayList<Coordinate> sonarArea= new ArrayList<>();
        int cRow = c.getRow();
        int cColumn = c.getColumn();
        //for the top area
        int topRow = -3;
        int topColumn = 0;
        for(int i = 1; i <= 4; i++){
            for(int j = 0; j < i * 2 - 1; j++){
                if(cRow + topRow >= 0 && cColumn + j - topColumn >= 0 && cRow + topRow <= this.height && cColumn + j - topColumn <= this.width){
                    Coordinate mySonar = new Coordinate(cRow + topRow, cColumn + j - topColumn);
                    sonarArea.add(mySonar);
                }
            }
            topRow++;
            topColumn++;
        }
        //for the bottom area
        int bottomRow = 1;
        int bottomColumn = 2;
        for(int i = 3; i > 0; i--){
            for(int j = 0; j < i * 2 - 1; j++){
                if(cRow + bottomRow >= 0 && cColumn + j - bottomColumn >= 0 && cRow + bottomRow <= this.height && cColumn + j - bottomColumn <= this.width){
                    Coordinate mySonar = new Coordinate(cRow + bottomRow, cColumn + j - bottomColumn);
                    sonarArea.add(mySonar);
                }
            }
            bottomRow++;
            bottomColumn--;
        }
        return sonarArea;
    }
    /**
     * get the sonar scan area's ship status
     * @param c the given coordinate
     * @return the hashmap of ship name and squares taken, null if coordinate is out of bound
     */
    public HashMap<String, Integer> sonarScan(Coordinate c){
        if(!isCoordinateOnBoard(c)){
            return null;
        }
        HashMap<String, Integer> scanResult = new HashMap<>();
        //initialize
        scanResult.put("Battleship", 0);
        scanResult.put("Carrier", 0);
        scanResult.put("Destroyer", 0);
        scanResult.put("Submarine", 0);
        ArrayList<Coordinate> sonarArea = getSonarArea(c);
        //check the ship name in the sonar area
        for(Coordinate sonar: sonarArea){
            if(this.getShipAt(sonar) != null){
                //update the squares that the type of ship takes
                scanResult.put(this.getShipAt(sonar).getName(), scanResult.get(this.getShipAt(sonar).getName()) + 1);
            }
        }
        return scanResult;
    }
    /**
    * check if the coordinate is on the board
     * @param c the given coordinate
    * @return true if the coordinate is on board, false otherwise
    */
    public boolean isCoordinateOnBoard(Coordinate c){
        if(c.getRow() > this.height || c.getRow() < 0 || c.getColumn() > this.width || c.getColumn() < 0){
            return false;
        }
        return true;
    }
}
