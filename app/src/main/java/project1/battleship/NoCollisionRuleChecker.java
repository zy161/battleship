package project1.battleship;

public class NoCollisionRuleChecker<T> extends PlacementRuleChecker<T> {
    public NoCollisionRuleChecker(PlacementRuleChecker<T> next) {
        super(next);
    }
    /**
     * check if the ship can be placed on the board without overlapping other ships
     * @param theShip the ship that's going to be placed on the board
     * @param theBoard the player's board
     * @return the error message string
     */
    @Override
    protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {
        Iterable<Coordinate> coords = theShip.getCoordinates();
        for(Coordinate c : coords){
            if(theBoard.whatIsAtForSelf(c) != null){
                return ("That placement is invalid: the ship overlaps another ship.");
            }
        }
        return null;
    }
}
