package project1.battleship;

public class V1ShipFactory implements AbstractShipFactory<Character>{
    /**
     * make ships based on the parameters
     * @param w the width of the ship
     * @param h the height of the ship
     * @param letter the letter represents the ship
     * @param name the name of the ship
     * @return the ship created
     */
    protected Ship<Character> createShip(Placement where, int w, int h, char letter, String name){
        if(where.getOrientation() != 'H' && where.getOrientation() != 'V'){
            throw new IllegalArgumentException("Invalid placement, with invalid orientation!");
        }
        if(where.getOrientation() == 'V'){
            return new RectangleShip<>(name, where.getCoordinate(), w, h, new SimpleShipDisplayInfo<>(letter, '*'), new SimpleShipDisplayInfo<>(null, letter));
        }
        else{
            return new RectangleShip<>(name, where.getCoordinate(), h, w, new SimpleShipDisplayInfo<>(letter, '*'), new SimpleShipDisplayInfo<>(null, letter));
        }
    }
    @Override
    public Ship<Character> makeSubmarine(Placement where) {
        return createShip(where, 1, 2, 's', "Submarine");
    }

    @Override
    public Ship<Character> makeBattleship(Placement where) {
        return createShip(where, 1, 4, 'b', "Battleship");
    }

    @Override
    public Ship<Character> makeCarrier(Placement where) {
        return createShip(where, 1, 6, 'c', "Carrier");
    }

    @Override
    public Ship<Character> makeDestroyer(Placement where) {
        return createShip(where, 1, 3, 'd', "Destroyer");
    }
}
