package project1.battleship;

import java.util.HashMap;
import java.util.HashSet;

public class TShapeShip<T> extends BasicShip<T> {
    final String name;
    @Override
    public String getName() {
        return name;
    }
    public TShapeShip(Placement p, ShipDisplayInfo<T> myDisplayInfo, ShipDisplayInfo<T> enemyDisplayInfo) {
        super(makeCoords(p), myDisplayInfo, enemyDisplayInfo, makeIndexes(p));
        this.name = "Battleship";
    }
    public TShapeShip(Placement p, T data, T onHit) {
        this(p, new SimpleShipDisplayInfo<T>(data, onHit), new SimpleShipDisplayInfo<T>(null, data));
    }

    //Battleships:
    //              *b      OR    B         Bbb        *b
    //              bbb           bb   OR    b     OR  bb
    //               1            b                     b
    //              234            2        432         4
    //                             31        1         13
    //                             4                    2
    //               Up          Right      Down      Left
    /**
     * make a coordinates' hash set based on the given placement for a battleship
     * @param p the ship's placement
     *
     * @return the hash map of coordinates that the ship occupies
     */
    static HashSet<Coordinate> makeCoords(Placement p){
        HashSet<Coordinate> myCoords = new HashSet<>();
        //need change here
        Coordinate upperLeft = p.getCoordinate();
        if(p.getOrientation() == 'U'){
            for(int i = 0; i < 3; i++){
                myCoords.add(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + i));
            }
            myCoords.add(new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 1));
        }
        else if(p.getOrientation() == 'R'){
            for(int i = 0; i < 3; i++){
                myCoords.add(new Coordinate(upperLeft.getRow() + i, upperLeft.getColumn()));
            }
            myCoords.add(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 1));
        }
        else if(p.getOrientation() == 'D'){
            for(int i = 0; i < 3; i++){
                myCoords.add(new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + i));
            }
            myCoords.add(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 1));
        }
        else{
            for(int i = 0; i < 3; i++){
                myCoords.add(new Coordinate(upperLeft.getRow() + i, upperLeft.getColumn() + 1));
            }
            myCoords.add(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn()));
        }
        return myCoords;
    }
    //Battleships:
    //              *b      OR    B         Bbb        *b
    //              bbb           bb   OR    b     OR  bb
    //               0            b                     b
    //              123            1        321         3
    //                             20        0         02
    //                             3                    1
    //               Up          Right      Down      Left
    /**
     * make a coordinates and indexes' hash map based on the given placement for a battleship
     * @param p the ship's placement
     *
     * @return the hash map of coordinates that the ship occupies and the corresponding indexes
     */
    static HashMap<Coordinate, Integer> makeIndexes(Placement p){
        HashMap<Coordinate, Integer> myIndex = new HashMap<>();
        Coordinate upperLeft = p.getCoordinate();
        char o = p.getOrientation();
        if(o == 'U'){
            Coordinate c0 = new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 1);
            myIndex.put(c0, 0);
            Coordinate c1 = new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn());
            myIndex.put(c1, 1);
            Coordinate c2 = new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 1);
            myIndex.put(c2, 2);
            Coordinate c3 = new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 2);
            myIndex.put(c3, 3);
        }
        else if(o == 'R'){
            Coordinate c0 = new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 1);
            myIndex.put(c0, 0);
            Coordinate c1 = new Coordinate(upperLeft.getRow(), upperLeft.getColumn());
            myIndex.put(c1, 1);
            Coordinate c2 = new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn());
            myIndex.put(c2, 2);
            Coordinate c3 = new Coordinate(upperLeft.getRow() + 2, upperLeft.getColumn());
            myIndex.put(c3, 3);
        }
        else if(o == 'D'){
            Coordinate c1 = new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 1);
            myIndex.put(c1, 0);
            Coordinate c2 = new Coordinate(upperLeft.getRow() , upperLeft.getColumn() + 2);
            myIndex.put(c2, 1);
            Coordinate c3 = new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 1);
            myIndex.put(c3, 2);
            Coordinate c4 = new Coordinate(upperLeft.getRow(), upperLeft.getColumn());
            myIndex.put(c4, 3);
        }
        else if(o == 'L'){
            Coordinate c0 = new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn());
            myIndex.put(c0, 0);
            Coordinate c1 = new Coordinate(upperLeft.getRow() + 2, upperLeft.getColumn() + 1);
            myIndex.put(c1, 1);
            Coordinate c2 = new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 1);
            myIndex.put(c2, 2);
            Coordinate c3 = new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 1);
            myIndex.put(c3, 3);
        }
        return myIndex;
    }
}
