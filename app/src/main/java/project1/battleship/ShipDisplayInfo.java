package project1.battleship;

public interface ShipDisplayInfo<T> {
    T getInfo(Coordinate where, boolean hit);
}
