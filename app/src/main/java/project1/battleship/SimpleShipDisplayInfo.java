package project1.battleship;

public class SimpleShipDisplayInfo<T> implements ShipDisplayInfo<T> {
    T myData;
    T onHit;
    public SimpleShipDisplayInfo(T d, T h){
        myData = d;
        onHit = h;
    }
    //display the board's info based on the hit status
    public T getInfo(Coordinate where, boolean hit){
        return hit ? onHit : myData;
    }
}
