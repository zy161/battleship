package project1.battleship;


public class Coordinate {
    private final int row;
    private final int column;
    public Coordinate(int r, int c){
        if (r < 0) {
            throw new IllegalArgumentException("BattleShipBoard's row must be positive but is " + r);
        }
        if (c < 0) {
            throw new IllegalArgumentException("BattleShipBoard's column must be positive but is " + c);
        }

        this.row = r;
        this.column = c;
    }
    /**
     * generate a coordinate based on the input string
     */
    public Coordinate(String descr) {
        String init = descr.toUpperCase();
        if(init.length() != 2){
            throw new IllegalArgumentException("Invalid input, which should be two character in the string!");
        }
        char rowLetter = init.charAt(0);
        char colLetter = init.charAt(1);
        if (rowLetter < 'A' || rowLetter > 'Z'){
            throw new IllegalArgumentException(rowLetter + " is an invalid row index which is out of bound!");
        }
        if (colLetter < '0' || colLetter > '9'){
            throw new IllegalArgumentException(colLetter + " is an invalid column index, out of bound!");
        }
        this.row = rowLetter - 'A';
        this.column = colLetter - '0';
    }
    public int getRow(){
        return row;
    }
    public int getColumn(){
        return column;
    }
    /**
     * compare if two coordinates are referring the same place on the board
     * @param o the object comparing
     */
    @Override
    public boolean equals(Object o) {
        if (o.getClass().equals(getClass())) { //Not 'o instanceof Coordinate' because if we later made a subclass of Coordinate
            Coordinate c = (Coordinate) o; //we might get them as equal incorrectly
            return row == c.row && column == c.column;
        }
        return false;
    }
    @Override
    public String toString() {
        return "(" + row + ", " + column + ")";
    }
    @Override
    public int hashCode() {
        return toString().hashCode();
    }

}
