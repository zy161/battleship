package project1.battleship;

import java.util.HashMap;
import java.util.HashSet;

public class SShapeShip<T> extends BasicShip<T> {
    final String name;
    @Override
    public String getName() {
        return name;
    }
    public SShapeShip(Placement p, ShipDisplayInfo<T> myDisplayInfo, ShipDisplayInfo<T> enemyDisplayInfo) {
        super(makeCoords(p), myDisplayInfo, enemyDisplayInfo, makeIndexes(p));
        this.name = "Carrier";
    }
    public SShapeShip(Placement p, T data, T onHit) {
        this(p, new SimpleShipDisplayInfo<>(data, onHit),
                new SimpleShipDisplayInfo<>(null, data));
    }

    //Carriers:
    //                  C                       C
    //                  c          *cccc        cc       * ccc
    //                  cc   OR    ccc      OR  cc   OR  cccc
    //                  cc                       c
    //                   c                       c
    //
    //                 Up           Right     Down          Left
    /**
     * make a coordinates' hash set based on the given placement for a carrier
     * @param p the ship's placement
     *
     * @return the hash set of coordinates that the ship occupies
     */
    static HashSet<Coordinate> makeCoords(Placement p){
        HashSet<Coordinate> myCoords = new HashSet<>();
        Coordinate upperLeft = p.getCoordinate();
        if(p.getOrientation() == 'U'){
            myCoords.add(upperLeft);
            for(int i = 2; i < 4; i++){
                for(int j = 0; j < 2; j++){
                    myCoords.add(new Coordinate(upperLeft.getRow() + i, upperLeft.getColumn() + j));
                }
            }
            myCoords.add(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn()));
            myCoords.add(new Coordinate(upperLeft.getRow() + 4, upperLeft.getColumn() + 1));
        }
        else if(p.getOrientation() == 'R'){
            for(int i = 0; i < 2; i++){
                for(int j = 1; j < 3; j++){
                    myCoords.add(new Coordinate(upperLeft.getRow() + i, upperLeft.getColumn() + j));
                }
            }
            myCoords.add(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn()));
            myCoords.add(new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 3));
            myCoords.add(new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 4));
        }
        else if(p.getOrientation() == 'D'){
            myCoords.add(upperLeft);
            for(int i = 1; i < 3; i++){
                for(int j = 0; j < 2; j++){
                    myCoords.add(new Coordinate(upperLeft.getRow() + i, upperLeft.getColumn() + j));
                }
            }
            myCoords.add(new Coordinate(upperLeft.getRow() + 3, upperLeft.getColumn() + 1));
            myCoords.add(new Coordinate(upperLeft.getRow() + 4, upperLeft.getColumn() + 1));
        }
        else{
            for(int i = 0; i < 2; i++){
                for(int j = 2; j < 4; j++){
                    myCoords.add(new Coordinate(upperLeft.getRow() + i, upperLeft.getColumn() + j));
                }
            }
            myCoords.add(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn()));
            myCoords.add(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 1));
            myCoords.add(new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 4));
        }
        return myCoords;
    }

    //Carriers:
    //                  C                       C
    //                  c          *cccc        cc       * ccc
    //                  cc   OR    ccc      OR  cc   OR  cccc
    //                  cc                       c
    //                   c                       c
    //                  0           3210        6         456
    //                  1          654          53      0123
    //                  24                      42
    //                  35                       1
    //                   6                       0
    //                 Up           Right     Down          Left
    /**
     * make a coordinates and indexes' hash map based on the given placement for a carrier
     * @param p the ship's placement
     *
     * @return the hash map of coordinates that the ship occupies and the corresponding indexes
     */
    static HashMap<Coordinate, Integer> makeIndexes(Placement p){
        HashMap<Coordinate, Integer> myIndex = new HashMap<>();
        Coordinate upperLeft = p.getCoordinate();
        char o = p.getOrientation();
        if(o == 'U'){
            Coordinate c0 = new Coordinate(upperLeft.getRow(), upperLeft.getColumn());
            myIndex.put(c0, 0);
            Coordinate c1 = new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn());
            myIndex.put(c1, 1);
            Coordinate c2 = new Coordinate(upperLeft.getRow() + 2, upperLeft.getColumn());
            myIndex.put(c2, 2);
            Coordinate c3 = new Coordinate(upperLeft.getRow() + 3, upperLeft.getColumn());
            myIndex.put(c3, 3);
            Coordinate c4 = new Coordinate(upperLeft.getRow() + 2, upperLeft.getColumn() + 1);
            myIndex.put(c4, 4);
            Coordinate c5 = new Coordinate(upperLeft.getRow() + 3, upperLeft.getColumn() + 1);
            myIndex.put(c5, 5);
            Coordinate c6 = new Coordinate(upperLeft.getRow() + 4, upperLeft.getColumn() + 1);
            myIndex.put(c6, 6);
        }
        else if(o == 'R'){
            Coordinate c0 = new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 4);
            myIndex.put(c0, 0);
            Coordinate c1 = new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 3);
            myIndex.put(c1, 1);
            Coordinate c2 = new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 2);
            myIndex.put(c2, 2);
            Coordinate c3 = new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 1);
            myIndex.put(c3, 3);
            Coordinate c4 = new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 2);
            myIndex.put(c4, 4);
            Coordinate c5 = new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 1);
            myIndex.put(c5, 5);
            Coordinate c6 = new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn());
            myIndex.put(c6, 6);
        }
        else if(o == 'D'){
            Coordinate c0 = new Coordinate(upperLeft.getRow() + 4, upperLeft.getColumn() + 1);
            myIndex.put(c0, 0);
            Coordinate c1 = new Coordinate(upperLeft.getRow() + 3, upperLeft.getColumn() + 1);
            myIndex.put(c1, 1);
            Coordinate c2 = new Coordinate(upperLeft.getRow() + 2, upperLeft.getColumn() + 1);
            myIndex.put(c2, 2);
            Coordinate c3 = new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 1);
            myIndex.put(c3, 3);
            Coordinate c4 = new Coordinate(upperLeft.getRow() + 2, upperLeft.getColumn());
            myIndex.put(c4, 4);
            Coordinate c5 = new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn());
            myIndex.put(c5, 5);
            Coordinate c6 = new Coordinate(upperLeft.getRow(), upperLeft.getColumn());
            myIndex.put(c6, 6);
        }
        else if(o == 'L'){
            Coordinate c0 = new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn());
            myIndex.put(c0, 0);
            Coordinate c1 = new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 1);
            myIndex.put(c1, 1);
            Coordinate c2 = new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 2);
            myIndex.put(c2, 2);
            Coordinate c3 = new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 3);
            myIndex.put(c3, 3);
            Coordinate c4 = new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 2);
            myIndex.put(c4, 4);
            Coordinate c5 = new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 3);
            myIndex.put(c5, 5);
            Coordinate c6 = new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 4);
            myIndex.put(c6, 6);
        }
        return myIndex;
    }
}
