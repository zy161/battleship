package project1.battleship;

public class V2ShipFactory extends V1ShipFactory{
    /**
     * make ships based on the parameters
     * @param letter the letter represents the ship
     * @param name the name of the ship
     * @return the ship created
     */
    protected Ship<Character> createShip(Placement where, char letter, String name){
        if(name.equals("Battleship")) {
            return new TShapeShip<>(where, new SimpleShipDisplayInfo<>(letter, '*'), new SimpleShipDisplayInfo<>(null, letter));
        }
        else if (name.equals("Carrier")){
            return new SShapeShip<>(where, new SimpleShipDisplayInfo<>(letter, '*'), new SimpleShipDisplayInfo<>(null, letter));
        }
        else {
            throw new IllegalArgumentException("" + "Should create battle ship or carriers but receive " + name);
        }
    }

    @Override
    public Ship<Character> makeBattleship(Placement where) {
        if(where.getOrientation() != 'U' && where.getOrientation() != 'R' && where.getOrientation() != 'D' && where.getOrientation() != 'L'){
            throw new IllegalArgumentException("Invalid placement, with invalid orientation!");
        }
        return createShip(where, 'b', "Battleship");
    }

    @Override
    public Ship<Character> makeCarrier(Placement where) {
        if(where.getOrientation() != 'U' && where.getOrientation() != 'R' && where.getOrientation() != 'D' && where.getOrientation() != 'L'){
            throw new IllegalArgumentException("Invalid placement, with invalid orientation!");
        }
        return createShip(where, 'c', "Carrier");
    }


}
