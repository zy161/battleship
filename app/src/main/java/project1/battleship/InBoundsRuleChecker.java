package project1.battleship;


public class InBoundsRuleChecker<T> extends PlacementRuleChecker<T> {
    public InBoundsRuleChecker(PlacementRuleChecker<T> next) {
        super(next);
    }
    /**
     * check if the ship can be placed on the board without passing the bounds
     * @param theShip the ship that's going to be placed on the board
     * @param theBoard the player's board
     * @return the error message string
     */
    @Override
    protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {
        Iterable<Coordinate> coords = theShip.getCoordinates();
        for(Coordinate c : coords){
            if(c.getColumn() < 0){
                return ("That placement is invalid: the ship goes off the left of the board.");
            }
            else if(c.getColumn() >= theBoard.getWidth()){
                return ("That placement is invalid: the ship goes off the right of the board.");
            }
            else if(c.getRow() < 0){
                return ("That placement is invalid: the ship goes off the top of the board.");
            }
            else if(c.getRow() >= theBoard.getHeight()){
                return ("That placement is invalid: the ship goes off the bottom of the board.");
            }
        }
        return null;
    }
}
