package project1.battleship;

import java.util.ArrayList;
import java.util.HashMap;

public abstract class BasicShip<T> implements Ship<T> {
    protected HashMap<Coordinate, Boolean> myPieces;
    protected HashMap<Coordinate, Integer> myCoordsIndex;
    protected ShipDisplayInfo<T> myDisplayInfo;
    protected ShipDisplayInfo<T> enemyDisplayInfo;
    public BasicShip(Iterable<Coordinate> where, ShipDisplayInfo<T> myDisplayInfo, ShipDisplayInfo<T> enemyDisplayInfo, HashMap<Coordinate, Integer> CoordsIndex){
        this.myPieces = new HashMap<>();
        this.myCoordsIndex = CoordsIndex;
        for(Coordinate c: where){
            this.myPieces.put(c, false);
        }
        this.myDisplayInfo = myDisplayInfo;
        this.enemyDisplayInfo = enemyDisplayInfo;
    }
    @Override
    public Iterable<Coordinate> getCoordinates() {
        return myPieces.keySet();
    }
    /**
     * find whether the given coordinate has a ship
     * @param where is the Coordinate to check if this Ship occupies
     * @return true if has ship, otherwise return false
     */
    @Override
    public boolean occupiesCoordinates(Coordinate where){
        return myPieces.containsKey(where);
    }
    protected void checkCoordinateInThisShip(Coordinate c){
        if(!this.occupiesCoordinates(c)) {
            throw new IllegalArgumentException("(" + c.getRow() + "," + c.getColumn() + ") is not a Coordinate on this ship!\n");
        }
    }
    /**
     * find whether the ship is sunk or not
     * @return true if ship sunk, otherwise return false
     */
    @Override
    public boolean isSunk(){
        for(Coordinate i : this.myPieces.keySet()){
            if(!myPieces.get(i)){
                return false;
            }
        }
        return true;
    }
    /**
     * record a hit at the ship with the given coordinate
     * @param where the given coordinate, check if it's occupied by a ship first
     */
    @Override
    public void recordHitAt(Coordinate where){
        checkCoordinateInThisShip(where);
        this.myPieces.put(where, true);
    }
    /**
     * check if it's hit at the ship with the given coordinate
     * @return boolean true if it's hit at the given coordinate, false otherwise
     * @param where the given coordinate, check if it's occupied by a ship first
     */
    @Override
    public boolean wasHitAt(Coordinate where){
        checkCoordinateInThisShip(where);
        return this.myPieces.get(where);
    }
    /**
     * return the view of the given coordinate
     * @param where the given coordinate
     * @param myShip whether it's for the player's view or enemy's view
     * @return T the display information
     */
    @Override
    public T getDisplayInfoAt(Coordinate where, boolean myShip){
        if (myShip) {
            return myDisplayInfo.getInfo(where, wasHitAt(where));
        }
        return enemyDisplayInfo.getInfo(where,wasHitAt(where));

    }
    /**
     * get the ship's hit information based on the index
     * @return the list of hit coordinates' indexes
     */
    @Override
    public ArrayList<Integer> getHitInfo(){
        ArrayList<Integer> hitInfo = new ArrayList<>();
        for(Coordinate c : myCoordsIndex.keySet()){
            if(this.wasHitAt(c)){
                hitInfo.add(myCoordsIndex.get(c));
            }
        }
        return hitInfo;
    }
    /**
     * get the relatively fixed coordinate on a ship based on the index
     * @param i the given index, to get the corresponding coordinate
     */
    @Override
    public Coordinate getCoordFromIndex(int i){
        for(Coordinate c : myCoordsIndex.keySet()){
            if(myCoordsIndex.get(c) == i){
                return c;
            }
        }
        return null;
    }
}
