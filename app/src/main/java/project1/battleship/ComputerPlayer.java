package project1.battleship;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.function.Function;

public class ComputerPlayer extends TextPlayer{
    private final Random rand;
    public ComputerPlayer(String playerName, Board<Character> theBoard, Reader inputSource, PrintStream out, V1ShipFactory factory1, V2ShipFactory factory2) {
        super(playerName, theBoard, inputSource, out, factory1, factory2);
        this.rand = new Random();
    }
    /**
     * randomly generate a placement for the computer player
     * @param shipName the ship's name
     * @return the randomly generated placement string
     */
    private String buildPlacement(String shipName){
        StringBuilder placementBuilder = new StringBuilder();
        String orient = null;
        //randomly generate a placement for computer
        if(shipName.equals("Submarine") || shipName.equals("Destroyer")){
            orient = "H";
        }
        else if(shipName.equals("Battleship") || shipName.equals("Carrier")){
            orient = "U";
        }
        char randRow = (char)(65 + rand.nextInt(20));
        int randColumn = rand.nextInt(10);
        placementBuilder.append(randRow);
        placementBuilder.append(randColumn);
        placementBuilder.append(orient);
        return placementBuilder.toString();
    }
    /**
     * place each ship in the shipsToPlace array
     *
     */
    public void doPlacementPhase() throws IOException{
        for(String shipName : shipsToPlace){
            //call doOnePlacement to place ships
            this.doOnePlacement(shipName, shipCreationFns.get(shipName));
        }
    }
    /**
     * Do one new Placement and add ship to the board and the toDisplay in view
     * @param createFn is an object an apply method that takes a Placement and returns a Ship<Character>
     * @throws IOException
     */
    @Override
    public void doOnePlacement(String shipName, Function<Placement, Ship<Character>> createFn) throws IOException {
        //Create a basic ship based on the location in that Placement
        while(true){
            try{
                String randPlacement = buildPlacement(shipName);
                Placement p = new Placement(randPlacement);
                Ship<Character> s = createFn.apply(p);
                String error = theBoard.tryAddShip(s);
                if (error == null) {
                    break; // accept the correct output
                }
            }
            catch (IllegalArgumentException e) {}
        }
    }
    /**
     * randomly choose a ship and move it on the board
     *
     */
    @Override
    public String doOneMove() throws IOException {
        Ship<Character> removing;
        //get the ship that should be moved based on the location in that Placement
        while(true){
            try{
                int randRow = rand.nextInt(20);
                int randColumn = rand.nextInt(10);
                Coordinate c = new Coordinate(randRow, randColumn);
                //get the ship information
                removing = theBoard.getShipAt(c);
                //check if the coordinate is a part of the player's ships
                if(removing != null){
                    break;
                }
            }
            catch (IllegalArgumentException e) {}
        }
        while(true){
            try{
                String randPlacement = buildPlacement(removing.getName());
                Placement p = new Placement(randPlacement);
                Ship<Character> s = shipCreationFns.get(removing.getName()).apply(p);
                //remove the old ship from theBoard's shipList
                theBoard.removeShip(removing);
                //place the new ship on the board
                String error = theBoard.tryAddShip(s);
                if (error == null) {
                    //need to change the new ship's hit status accordingly
                    ArrayList<Integer> hitStatus = removing.getHitInfo();
                    for(Integer i : hitStatus){
                        s.recordHitAt(s.getCoordFromIndex(i));
                    }
                    this.move -= 1;
                    break;
                }
                else {
                    //place the old ship back if failed to place the new ship
                    theBoard.tryAddShip(removing);
                }
            }
            catch(IllegalArgumentException e){}
        }
        out.println("Player " + this.name + " used a special action!");
        return null;
    }
    /**
     * randomly generate a coordinate and do sonar scan for it
     * @param enemyBoard the enemy's board
     */
    @Override
    public void doOneSonar(Board<Character> enemyBoard) throws IOException{
            try{
                int randRow = rand.nextInt(20);
                int randColumn = rand.nextInt(10);
                Coordinate c = new Coordinate(randRow, randColumn);
                HashMap<String, Integer> scanInfo = enemyBoard.sonarScan(c);
                this.sonar -= 1;
            }
            catch(IllegalArgumentException e){}
        out.println("Player " + this.name + " used a special action!");
    }
    /**
     * randomly generate a coordinate and fire at it
     * @param enemyBoard the enemy's board
     */
    @Override
    public void doOneFire(Board<Character> enemyBoard)throws IOException{
        Coordinate fireCoord;
        //randomly generate a coordinate to fire at
        int randRow = rand.nextInt(20);
        int randColumn = rand.nextInt(10);
        fireCoord = new Coordinate(randRow, randColumn);
        Ship<Character> fireShip = enemyBoard.fireAt(fireCoord);
        if(fireShip != null){
            out.println("The computer player hit your " + fireShip.getName() + " at" + fireCoord.toString() + "!");
        }
    }
    /**
     * use all the special actions first and then do fire randomly
     * @param enemyBoard the enemy's board
     */
    @Override
    public void doNewMoves(Board<Character> enemyBoard) throws IOException{
        if(this.move > 0){
            doOneMove();
        }
        else if(this.sonar > 0){
            doOneSonar(enemyBoard);
        }
        else{
            //directly go to fire stage
            doOneFire(enemyBoard);
        }
    }
    /**
     * play one round of game and display no guidance
     * @param enemyBoard the enemy's board
     * @param enemyName the enemy's name
     */
    @Override
    public void playOneTurn(Board<Character> enemyBoard, String enemyName) throws IOException{
        //BoardTextView enemyView = new BoardTextView(enemyBoard);
        //out.print(view.displayMyBoardWithEnemyNextToIt(enemyView, "Your Ocean", "Player " + enemyName + "'s ocean"));
        //String turnPrompt = "Player " + this.name + "'s turn:";
        //out.println(turnPrompt);
        doNewMoves(enemyBoard);
    }
}
