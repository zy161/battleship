package project1.battleship;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.function.Function;

public class TextPlayer {
    final Board<Character> theBoard;
    final BoardTextView view;
    final BufferedReader inputReader;
    final PrintStream out;
    final AbstractShipFactory<Character> shipFactoryV1;
    final AbstractShipFactory<Character> shipFactoryV2;
    final ArrayList<String> shipsToPlace;
    final HashMap<String, Function<Placement, Ship<Character>>> shipCreationFns;
    final String name;
    public int move = 3;
    public int sonar = 3;

    public TextPlayer(String playerName, Board<Character> theBoard, Reader inputSource, PrintStream out, V1ShipFactory factory1, V2ShipFactory factory2) {
        this.name = playerName;
        this.theBoard = theBoard;
        this.view = new BoardTextView(theBoard);
        this.inputReader = new BufferedReader(inputSource);
        this.out = out;
        this.shipFactoryV1 = factory1;
        this.shipFactoryV2 = factory2;
        this.shipsToPlace = new ArrayList<>();
        this.shipCreationFns = new HashMap<>();
        this.setupShipCreationList();
        this.setupShipCreationMap();
    }
    //set up the ship creation functions map
    protected void setupShipCreationMap(){
        shipCreationFns.put("Submarine", (p) -> shipFactoryV1.makeSubmarine(p));
        shipCreationFns.put("Destroyer", (p) -> shipFactoryV1.makeDestroyer(p));
        shipCreationFns.put("Battleship", (p) -> shipFactoryV2.makeBattleship(p));
        shipCreationFns.put("Carrier", (p) -> shipFactoryV2.makeCarrier(p));
    }
    //set up the ship creation list
    protected void setupShipCreationList(){
        shipsToPlace.addAll(Collections.nCopies(2, "Submarine"));
        shipsToPlace.addAll(Collections.nCopies(3, "Destroyer"));
        shipsToPlace.addAll(Collections.nCopies(3, "Battleship"));
        shipsToPlace.addAll(Collections.nCopies(2, "Carrier"));
    }
    //read the input string and make a placement based on it
    public Placement readPlacement(String prompt) throws IOException {
        out.println(prompt);
        String s = inputReader.readLine();
        return new Placement(s);
    }
    //print the guidance and do placements for the ships
    public void doPlacementPhase() throws IOException{
        //print the instructions message (from the README, but also shown again near the top of this file)
        out.print("--------------------------------------------------------------------------------\n" +
                "Player " + this.name + ": you are going to place the following ships (which are all\n" +
                "rectangular). For each ship, type the coordinate of the upper left\n" +
                "side of the ship, followed by either H (for horizontal) or V (for\n" +
                "vertical).  For example M4H would place a ship horizontally starting\n" +
                "at M4 and going to the right.  You have\n" +
                "\n" +
                "2 \"Submarines\" ships that are 1x2 \n" +
                "3 \"Destroyers\" that are 1x3\n" +
                "3 \"Battleships\" that are 1x4\n" +
                "2 \"Carriers\" that are 1x6\n");
        out.print("--------------------------------------------------------------------------------\n");
        //display the starting (empty) board and every time we place a ship
        out.print(this.view.displayMyOwnBoard());
        for(String shipName : shipsToPlace){
            out.print("--------------------------------------------------------------------------------\n");
            //call doOnePlacement to place ships
            this.doOnePlacement(shipName, shipCreationFns.get(shipName));
        }
    }
    /**
     * Do one new Placement and add ship to the board and the toDisplay in view
     * @param createFn is an object an apply method that takes a Placement and returns a Ship<Character>
     * @throws IOException
     */
    public void doOnePlacement(String shipName, Function<Placement, Ship<Character>> createFn) throws IOException {
        //read a Placement
        String prompt = "Player " + this.name + " where do you want to place a " + shipName + "?";
        //Create a basic ship based on the location in that Placement(orientation doesn't matter yet)
        while(true){
            try{
                Placement p = readPlacement(prompt);
                Ship<Character> s = createFn.apply(p);
                String error = theBoard.tryAddShip(s);
                if (error == null) {
                    break; // accept the correct output
                }
                else {
                    out.println(error);
                }
            }
            catch (IllegalArgumentException e) {
                out.println(e.getMessage());
            }
        }
        //Print out the board (to out, not to System.out)
        out.println("Current ocean:");
        out.print(this.view.displayMyOwnBoard());
    }
    /**
     * get a coordinate based on the input and get the ship that occupies the coordinate
     * get a placement based on the input and move the ship to the new placement
     * @return re-prompt for new action if the coordinate provided is invalid
     */
    public String doOneMove() throws IOException {
        Ship<Character> removing = null;
        //ask for a coordinate input
        out.println("Player " + this.name + " which ship do you want to move?");
        //Create a new ship based on the location in that Placement(orientation doesn't matter yet)
        try{
            Coordinate c = new Coordinate(inputReader.readLine());
            //get the ship information
            removing = theBoard.getShipAt(c);
            //check if the coordinate is a part of the player's ships
            if(removing == null){
                out.println("There is no ship at this coordinate, please select your action again!");
                return("Invalid coordinate selection.");
            }
        }
        catch (IllegalArgumentException e) {
            out.println(e.getMessage());
        }
        //get a new placement
        String prompt = "Player " + this.name + " where do you want to place this " + removing.getName() + "?";
        while(true){
            try{
                Placement p = readPlacement(prompt);
                Ship<Character> s = shipCreationFns.get(removing.getName()).apply(p);
                //remove the old ship from theBoard's shipList
                theBoard.removeShip(removing);
                //place the new ship on the board
                String error = theBoard.tryAddShip(s);
                if (error == null) {
                    //need to change the new ship's hit status accordingly
                    ArrayList<Integer> hitStatus = removing.getHitInfo();
                    for(Integer i : hitStatus){
                        s.recordHitAt(s.getCoordFromIndex(i));
                    }
                    this.move -= 1;
                    break;
                }
                else {
                    out.println(error);
                    //place the old ship back if failed to place the new ship
                    theBoard.tryAddShip(removing);
                }
            }
            catch(IllegalArgumentException e){
                out.println(e.getMessage());
            }
        }
        //Print out the board (to out, not to System.out)
        out.println("Current ocean:");
        out.print(this.view.displayMyOwnBoard());
        return null;
    }
    /**
     * get a coordinate based on the input and do sonar scan for it
     * @param enemyBoard the enemy's board
     */
    public void doOneSonar(Board<Character> enemyBoard) throws IOException{
        //read a Placement
        String prompt = "Player " + this.name + " where do you want to do a sonar scan?";
        while(true){
            try{
                out.println(prompt);
                Coordinate c = new Coordinate(inputReader.readLine());
                HashMap<String, Integer> scanInfo = enemyBoard.sonarScan(c);
                if(scanInfo == null){
                    out.println("The coordinate is out of bound, please select a valid coordinate!");
                }
                else {
                    out.println("---------------------------------------------------------------------------");
                    for(String shipName: scanInfo.keySet()){
                        out.println(shipName + " occupy " + scanInfo.get(shipName) +" squares");
                    }
                    this.sonar -= 1;
                    break;
                }
            }
            catch(IllegalArgumentException e){
                out.println(e.getMessage() + " Please chose a coordinate again!");
            }
        }

    }
    /**
     * get a coordinate based on the input and fire at it
     * @param enemyBoard the enemy's board
     */
    public void doOneFire(Board<Character> enemyBoard)throws IOException{
        String firePrompt = "Player " + this.name + " where do you want to fire?";
        Coordinate fireCoord;
        //keep asking the player to give a coordinate until it's valid
        while(true){
            try {
                out.println(firePrompt);
                String input = inputReader.readLine();
                if ((input == null)) {
                    throw new EOFException("Receive an EOF when reading fire coordinate");
                }
                fireCoord = new Coordinate(input);
                if(!theBoard.isCoordinateOnBoard(fireCoord)){
                    out.println("Invalid coordinate, which is out of bound! Please select a valid coordinate again.");
                }
                else{
                    break;
                }
            } catch (IllegalArgumentException e) {
                out.println(e.getMessage());
            }
        }
        Ship<Character> fireShip = enemyBoard.fireAt(fireCoord);
        if(fireShip == null){
            out.println("You missed!");
        }
        else{
            out.println("You hit a " + fireShip.getName() + " !");
        }
    }
    /**
     * let the player choose which action to do each turn
     * @param enemyBoard the enemy's board
     */
    public void doNewMoves(Board<Character> enemyBoard) throws IOException{
        if(this.move > 0 || this.sonar > 0){
            out.print("---------------------------------------------------------------------------\n" +
                    "Possible actions for Player " + this.name + ":\n" +
                    "\n" +
                    " F Fire at a square\n" +
                    " M Move a ship to another square ("+ this.move + " remaining)\n" +
                    " S Sonar scan ("+ this.sonar + " remaining)\n" +
                    "\n" +
                    "Player " + this.name + ", what would you like to do?\n");
            while(true){
                try{
                    String choice = readCommand();
                    if(choice.equals("M")){//for move
                        String moveAction = doOneMove();
                        if(moveAction == null){
                            break;
                        }
                    }
                    else if(choice.equals("S")){//for sonar
                        doOneSonar(enemyBoard);
                        break;
                    }
                    else if(choice.equals("F")) {//for fire
                        doOneFire(enemyBoard);
                        break;
                    }
                }
                catch (Exception e) {
                    out.println(e.getMessage());
                }
            }
        }
        else{
            //directly go to fire stage
            doOneFire(enemyBoard);
        }
    }
    public String readCommand() throws IOException{
        String init = inputReader.readLine();
        String choice = init.toUpperCase();
        if(choice == null) {
            throw new EOFException("Receive an EOF when there should not be that way.");
        }
        if(choice.length() != 1 || (!choice.equals("F") && !choice.equals("M") && !choice.equals("S"))){
            throw new IllegalArgumentException("Command should be one character long, and must be from 'F', 'M' and 'S'.");
        }
        if (choice.equals("M") && move == 0) {
            throw new IllegalArgumentException("There's no move remaining, please choose again.");
        }
        if (choice.equals("S") && sonar == 0) {
            throw new IllegalArgumentException("There's no sonar remaining, please choose again.");
        }
        return choice;
    }
    /**
     * play one round of game and display guidance
     * @param enemyBoard the enemy's board
     * @param enemyName the enemy's name
     */
    public void playOneTurn(Board<Character> enemyBoard, String enemyName) throws IOException{
        BoardTextView enemyView = new BoardTextView(enemyBoard);
        out.print(view.displayMyBoardWithEnemyNextToIt(enemyView, "Your Ocean", "Player " + enemyName + "'s ocean"));
        String turnPrompt = "Player " + this.name + "'s turn:";
        out.println(turnPrompt);
        doNewMoves(enemyBoard);
    }
}
