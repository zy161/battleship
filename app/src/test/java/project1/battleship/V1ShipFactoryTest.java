package project1.battleship;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class V1ShipFactoryTest {
    private void checkShip(Ship<Character> testShip, String expectedName, char expectedLetter, ArrayList<Coordinate> expectedLocs){
        assertEquals(testShip.getName(), expectedName);
        for(Coordinate c: expectedLocs){
            assertEquals(testShip.getDisplayInfoAt(c, true), expectedLetter);
        }
    }
    private ArrayList<Coordinate> getCoordinatesHelper(Coordinate upperLeft, int w, int h){
        ArrayList<Coordinate> coordsList=new ArrayList<>();
        for(int i = 0; i < w; ++i){
            for(int j = 0; j < h; ++j){
                coordsList.add(new Coordinate(upperLeft.getRow() + j, upperLeft.getColumn() + i));
            }
        }
        return coordsList;
    }
    @Test
    void test_make_ships(){
        V1ShipFactory shipFactory = new V1ShipFactory();
        Ship<Character> s1 = shipFactory.makeSubmarine(new Placement("A4V"));
        checkShip(s1, "Submarine", 's', getCoordinatesHelper(new Coordinate(0, 4), 1,2));
        Ship<Character> s2 = shipFactory.makeSubmarine(new Placement("B4H"));
        checkShip(s2, "Submarine", 's', getCoordinatesHelper(new Coordinate(1, 4), 2,1));
        Ship<Character> s3 = shipFactory.makeCarrier(new Placement("C1V"));
        checkShip(s3, "Carrier", 'c', getCoordinatesHelper(new Coordinate(2, 1), 1,6));
        Ship<Character> s4 = shipFactory.makeCarrier(new Placement("G5H"));
        checkShip(s4, "Carrier", 'c', getCoordinatesHelper(new Coordinate(6, 5), 6,1));
        Ship<Character> s5 = shipFactory.makeBattleship(new Placement("D0V"));
        checkShip(s5, "Battleship", 'b', getCoordinatesHelper(new Coordinate(3, 0), 1,4));
        Ship<Character> s6 = shipFactory.makeBattleship(new Placement("E9H"));
        checkShip(s6, "Battleship", 'b', getCoordinatesHelper(new Coordinate(4, 9), 4,1));
        Ship<Character> s7 = shipFactory.makeDestroyer(new Placement("F3V"));
        checkShip(s7, "Destroyer", 'd', getCoordinatesHelper(new Coordinate(5, 3), 1,3));
        Ship<Character> s8 = shipFactory.makeDestroyer(new Placement("I8H"));
        checkShip(s8, "Destroyer", 'd', getCoordinatesHelper(new Coordinate(8, 8), 3,1));
    }

}
