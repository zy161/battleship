package project1.battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PlacementTest {
    @Test
    public void test_where_and_orientation() {
        Coordinate c1 = new Coordinate(10, 20);
        Placement p1 = new Placement(c1, 'H');
        assertEquals(c1, p1.getCoordinate());
        assertEquals('H', p1.getOrientation());
    }
    @Test
    public void test_equals(){
        Coordinate c1 = new Coordinate(10, 20);
        Coordinate c2 = new Coordinate(1, 20);
        Coordinate c3 = new Coordinate(1, 20);
        Placement p1 = new Placement(c1, 'H');
        Placement p2 = new Placement(c1, 'h');
        Placement p3 = new Placement(c1, 'V');
        Placement p4 = new Placement(c2, 'v');
        Placement p5 = new Placement(c3, 'V');
        assertEquals(p1, p2);
        assertEquals(p4,p5);
        assertNotEquals(p1, p3);
        assertNotEquals(p3, p4);
        assertNotEquals(p2,p4);
    }
    @Test
    public void test_hashCode() {
        Coordinate c1 = new Coordinate(10, 20);
        Coordinate c2 = new Coordinate(1, 20);
        Coordinate c3 = new Coordinate(1, 20);
        Placement p1 = new Placement(c1, 'H');
        Placement p2 = new Placement(c1, 'h');
        Placement p3 = new Placement(c1, 'V');
        Placement p4 = new Placement(c2, 'v');
        Placement p5 = new Placement(c3, 'v');
        assertEquals(p1.hashCode(), p2.hashCode());
        assertEquals(p4.hashCode(), p5.hashCode());
        assertNotEquals(p1.hashCode(), p3.hashCode());
        assertNotEquals(p1.hashCode(), p4.hashCode());
    }
    @Test
    void test_string_constructor_valid_cases() {
        Placement p1 = new Placement("B3h");
        assertEquals(1, p1.getCoordinate().getRow());
        assertEquals(3, p1.getCoordinate().getColumn());
        assertEquals('H', p1.getOrientation());
        Placement p2 = new Placement("D5V");
        assertEquals(3, p2.getCoordinate().getRow());
        assertEquals(5, p2.getCoordinate().getColumn());
        assertEquals('V', p2.getOrientation());
        Placement p3 = new Placement("A9H");
        assertEquals(0, p3.getCoordinate().getRow());
        assertEquals(9, p3.getCoordinate().getColumn());
        assertEquals('H', p3.getOrientation());
        Placement p4 = new Placement("Z0v");
        assertEquals(25, p4.getCoordinate().getRow());
        assertEquals(0, p4.getCoordinate().getColumn());
        assertEquals('V', p4.getOrientation());

    }
    @Test
    public void test_string_constructor_error_cases() {
        assertThrows(IllegalArgumentException.class, () -> new Placement("000"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("AA0"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("@0V"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("V0A"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("A0/"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("A0VH"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("A"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("A12"));
    }
}
