package project1.battleship;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static project1.battleship.RectangleShip.makeCoords;

public class RectangleShipTest {
    @Test
    void test_Make_Coords(){
        HashSet<Coordinate> expected = new HashSet<>();
        expected.add(new Coordinate(1,1));
        expected.add(new Coordinate(1,2));
        Coordinate upperLeft = new Coordinate(1,1);
        HashSet<Coordinate> actual = makeCoords(upperLeft, 2, 1);
        assertEquals(expected, actual);
    }
    @Test
    void test_BS_constructor(){
        Coordinate c1 = new Coordinate(1,1);
        Coordinate c2 = new Coordinate(1,2);
        HashMap<Coordinate, Boolean> expected = new HashMap<>();
        expected.put(c1,false);
        BasicShip<Character> bs1 = new RectangleShip<>(c1, 's', '*');
        HashMap<Coordinate, Boolean> expected2 = new HashMap<>();
        expected2.put(c1, false);
        expected2.put(c2, false);
        BasicShip<Character> bs2 = new RectangleShip<>("submarine", new Coordinate(1,1), 2,1, new SimpleShipDisplayInfo<Character>('s', '*'), new SimpleShipDisplayInfo<Character>('X', 's'));
        assertEquals(bs1.myPieces, expected);
        assertEquals(bs2.myPieces, expected2);
    }
    @Test
    void test_BS_Iterator_Constructor(){
        Coordinate c1 = new Coordinate(1,1);
        Coordinate c2 = new Coordinate(1,2);
        HashMap<Coordinate, Boolean> expected2 = new HashMap<>();
        expected2.put(c1, false);
        expected2.put(c2, false);
        BasicShip<Character> bs2 = new RectangleShip<>("submarine", new Coordinate(1,1), 2,1, new SimpleShipDisplayInfo<Character>('s', '*'), new SimpleShipDisplayInfo<Character>('X', 's'));
        assertEquals(bs2.myPieces, expected2);
    }
    @Test
    void test_RS_constructor(){
        Coordinate c1 = new Coordinate(1,1);
        RectangleShip<Character> r1 = new RectangleShip<>("submarine", c1,2,1, 's', '*');
        assertEquals("submarine", r1.getName());
        assertTrue(r1.occupiesCoordinates(new Coordinate(1,1)));
        assertTrue(r1.occupiesCoordinates(new Coordinate(1,2)));
        assertFalse(r1.occupiesCoordinates(new Coordinate(1,3)));
    }
    @Test
    void test_isSunk(){
        BasicShip<Character> bs = new RectangleShip<>("submarine", new Coordinate(1,1),2, 1, 's', '*');
        assertThrows(IllegalArgumentException.class,
                ()->bs.checkCoordinateInThisShip(new Coordinate(0,0)));
        bs.recordHitAt(new Coordinate(1,1));
        assertFalse(bs.isSunk());
        bs.recordHitAt(new Coordinate(1,2));
        assertTrue(bs.isSunk());
    }
    @Test
    void test_record_hit(){
        BasicShip<Character> bs = new RectangleShip<>("submarine", new Coordinate(1,1),2, 1, 's', '*');
        bs.recordHitAt(new Coordinate(1,1));
        assertTrue(bs.myPieces.get(new Coordinate(1,1)));
        assertFalse(bs.myPieces.get(new Coordinate(1,2)));
    }
    @Test
    void test_was_hit(){
        BasicShip<Character> bs = new RectangleShip<>("submarine", new Coordinate(1,1),2, 1, 's', '*');
        bs.recordHitAt(new Coordinate(1,1));
        assertTrue(bs.wasHitAt(new Coordinate(1,1)));
        assertFalse(bs.wasHitAt(new Coordinate(1,2)));
    }
    @Test
    void test_get_display_info(){
        BasicShip<Character> bs = new RectangleShip<>("submarine", new Coordinate(1,1),2, 1, 's', '*');
        assertEquals('s', bs.getDisplayInfoAt(new Coordinate(1,1), true));
        bs.recordHitAt(new Coordinate(1,1));
        assertEquals('*', bs.getDisplayInfoAt(new Coordinate(1,1), true));
        assertEquals('s', bs.getDisplayInfoAt(new Coordinate(1,2), true));
    }
    @Test
    void test_get_iterator_coords(){
        BasicShip<Character> bs = new RectangleShip<>("submarine", new Coordinate(1,1),2, 1, 's', '*');
        Set<Coordinate> myset = new HashSet<Coordinate>();
        myset.add(new Coordinate(1,1));
        myset.add(new Coordinate(1,2));
        assertEquals(myset, bs.getCoordinates());
    }

}
