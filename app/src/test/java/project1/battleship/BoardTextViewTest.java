package project1.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
public class BoardTextViewTest {
    private void emptyBoardHelper(int w, int h, String expectedHeader, String expectedBody){
        Board<Character> b1 = new BattleShipBoard<>(w, h, 'X');
        BoardTextView view = new BoardTextView(b1);
        assertEquals(expectedHeader, view.makeHeader());
        String expected = expectedHeader + expectedBody + expectedHeader;
        assertEquals(expected, view.displayMyOwnBoard());
    }
    @Test
    public void test_display_empty_2by2() {
        String Body=
                "A  |  A\n"+
                "B  |  B\n";
        emptyBoardHelper(2,2, "  0|1\n", Body);
    }
    @Test
    public void test_display_empty_3by2(){
        String Body=
                "A  | |  A\n"+
                "B  | |  B\n";
        emptyBoardHelper(3,2, "  0|1|2\n", Body);
    }
    @Test
    public void test_display_empty_3by5(){
        String Body=
                "A  | |  A\n"+
                "B  | |  B\n"+
                "C  | |  C\n"+
                "D  | |  D\n"+
                "E  | |  E\n";
        emptyBoardHelper(3,5, "  0|1|2\n", Body);
    }
    @Test
    public void test_invalid_board_size() {
        Board<Character> wideBoard = new BattleShipBoard<>(11,20, 'X');
        Board<Character> tallBoard = new BattleShipBoard<>(10,27, 'X');
        //you should write two assertThrows here
        assertThrows(IllegalArgumentException.class, () -> new BoardTextView(wideBoard));
        assertThrows(IllegalArgumentException.class, () -> new BoardTextView(tallBoard));
    }
    @Test
    public void test_board_view_with_ships(){
        Board<Character> b1 = new BattleShipBoard<>(4,3, 'X');
        b1.tryAddShip(new RectangleShip<Character>(new Coordinate(1,1), 's', '*'));
        BoardTextView bv1 = new BoardTextView(b1);
        String expectedView =
                "  0|1|2|3\n" +
                "A  | | |  A\n" +
                "B  |s| |  B\n" +
                "C  | | |  C\n" +
                "  0|1|2|3\n";
        assertEquals(bv1.displayMyOwnBoard(), expectedView);
    }
}
