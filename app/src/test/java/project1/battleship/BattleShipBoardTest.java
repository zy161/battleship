package project1.battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class BattleShipBoardTest {
    @Test
    public void test_width_and_height() {
        Board<Character> b1 = new BattleShipBoard<>(10, 20, 'X');
        assertEquals(10, b1.getWidth());
        assertEquals(20, b1.getHeight());
    }
    @Test
    public void test_invalid_dimensions() {
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<>(10, 0, 'X'));
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<>(0, 20, 'X'));
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<>(10, -5, 'X'));
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<>(-8, 20, 'X'));
    }
    private <T> void checkWhatIsAtBoard(BattleShipBoard<T> b, T[][] expected){
        for(int i = 0; i < b.getWidth(); ++i){
            for(int j = 0; j< b.getHeight(); ++j){
                T temp = b.whatIsAtForSelf((new Coordinate(i, j)));
                if(temp != expected[i][j]){
                    throw new IllegalArgumentException("Expected" + expected[i][j] + "at ("+ i +","+ j +") but get " + temp + "\n");
                }
            }
        }
    }
    @Test
    public void test_board_occupation(){
        //make a BattleShipBoard, check that it has no ships anywhere
        BattleShipBoard<Character> B1 = new BattleShipBoard<>(2,2, 'X');
        Character[][]  expected=new Character[2][2];
        checkWhatIsAtBoard(B1, expected);
        //start adding ships and make sure the right coordinates return the Character 's'.
        B1.tryAddShip(new RectangleShip<Character>(new Coordinate(0,0), 's', '*'));
        checkWhatIsAtBoard(B1, new Character[][]{{'s', null},{null, null}});
        B1.tryAddShip(new RectangleShip<Character>(new Coordinate(1,0), 's', '*'));
        checkWhatIsAtBoard(B1, new Character[][]{{'s', null},{'s', null}});
        B1.tryAddShip(new RectangleShip<Character>(new Coordinate(1,1), 's', '*'));
        checkWhatIsAtBoard(B1, new Character[][]{{'s', null},{'s', 's'}});
        B1.tryAddShip(new RectangleShip<Character>(new Coordinate(0,1), 's', '*'));
        checkWhatIsAtBoard(B1, new Character[][]{{'s', 's'},{'s', 's'}});
    }
    @Test
    void test_fireAt(){
        //use assertSame to check the return value of fireAt
        //assertSame checks if they are exactly the same object (pointer equality)
    }

}