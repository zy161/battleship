package project1.battleship;

import org.junit.jupiter.api.Test;

import java.io.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TextPlayerTest {
    @Test
    void test_read_placement() throws IOException {
        //This class is a lot like a StringStream in C++, but only for reading.
        StringReader sr = new StringReader("B2V\nC8H\na4v\n");
        //collect the output
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        //ps is a PrintStream (looks like System.out) which writes its data into bytes instead of to the screen.
        PrintStream ps = new PrintStream(bytes, true);
        Board<Character> b = new BattleShipBoard<>(10, 20, 'X');
        TextPlayer player = new TextPlayer("A", b, new BufferedReader(sr), ps, new V1ShipFactory(), new V2ShipFactory());
        String prompt = "Please enter a location for a ship:";
        Placement[] expected = new Placement[3];
        expected[0] = new Placement(new Coordinate(1, 2), 'V');
        expected[1] = new Placement(new Coordinate(2, 8), 'H');
        expected[2] = new Placement(new Coordinate(0, 4), 'V');
        for (int i = 0; i < expected.length; i++) {
            Placement p = player.readPlacement(prompt);
            assertEquals(p, expected[i]); //did we get the right Placement back
            assertEquals(prompt + System.lineSeparator(), bytes.toString()); //should have printed prompt and newline
            bytes.reset(); //clear out bytes for next time around
        }
    }
//    @Test
//    void test_do_placement() throws IOException{
//        StringReader sr = new StringReader("B2V");
//        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//        PrintStream ps = new PrintStream(bytes, true);
//        Board<Character> b = new BattleShipBoard<>(10, 20);
//        TextPlayer player = new TextPlayer("A", b, new BufferedReader(sr), ps, new V1ShipFactory());
//        player.doOnePlacement("Submarine", (p)->shipFactory.makeSubmarine(p));
//        String promptExpected = "Player " + player.name + " Where would you like to put your ship?";
//        String expected = promptExpected + '\n' +
//                "  0|1|2|3|4|5|6|7|8|9\n" +
//                "A  | | | | | | | | |  A\n" +
//                "B  | |s| | | | | | |  B\n" +
//                "C  | |s| | | | | | |  C\n" +
//                "D  | |s| | | | | | |  D\n" +
//                "E  | | | | | | | | |  E\n" +
//                "F  | | | | | | | | |  F\n" +
//                "G  | | | | | | | | |  G\n" +
//                "H  | | | | | | | | |  H\n" +
//                "I  | | | | | | | | |  I\n" +
//                "J  | | | | | | | | |  J\n" +
//                "K  | | | | | | | | |  K\n" +
//                "L  | | | | | | | | |  L\n" +
//                "M  | | | | | | | | |  M\n" +
//                "N  | | | | | | | | |  N\n" +
//                "O  | | | | | | | | |  O\n" +
//                "P  | | | | | | | | |  P\n" +
//                "Q  | | | | | | | | |  Q\n" +
//                "R  | | | | | | | | |  R\n" +
//                "S  | | | | | | | | |  S\n" +
//                "T  | | | | | | | | |  T\n"+
//                "  0|1|2|3|4|5|6|7|8|9\n";
//        assertEquals(expected, bytes.toString());
//    }
}
