package project1.battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SimpleShipDisplayInfoTest {
    @Test
    void test_ship_display_info(){
        SimpleShipDisplayInfo<Character> myDisplay = new SimpleShipDisplayInfo<>('s', '*');
        Character infoTrue = myDisplay.getInfo(new Coordinate(1, 1), true);
        Character infoFalse = myDisplay.getInfo(new Coordinate(1, 1), false);
        assertEquals('*', infoTrue);
        assertEquals('s', infoFalse);
    }
}
